from sklearn.tree import DecisionTreeClassifier

# Задаем вероятности состояний рынка
probabilities = [0.25, 0.30, 0.45]

# Задаем выигрыши и убытки для каждой культуры в каждом состоянии рынка
# (прибыль для кукурузы, прибыль для соевых бобов)
profits_losses = [
    [30000, 10000],  # Цены возрастут
    [0, 0],          # Цены останутся на том же уровне
    [-35000, -5000]  # Цены упадут
]

# Вычисляем ожидаемые значения для каждой культуры
expected_values = []
for i in range(len(probabilities)):
    expected_value_corn = probabilities[i] * profits_losses[i][0]
    expected_value_soybeans = probabilities[i] * profits_losses[i][1]
    expected_values.append((expected_value_corn, expected_value_soybeans))

# Определяем лучшую культуру для каждого состояния рынка
best_crops = []
for values in expected_values:
    best_crop = "Кукуруза" if values[0] > values[1] else "Соевые бобы"
    best_crops.append(best_crop)

# Выводим решение для каждого состояния рынка
for i in range(len(probabilities)):
    print(f"Если цены {['возрастут', 'останутся на том же уровне', 'упадут'][i]}:")
    print(f"Рекомендуемая культура: {best_crops[i]}")
    print()

# Определяем лучшую культуру для фермера
best_crop_overall = "Кукуруза" if sum([values[0] for values in expected_values]) > sum([values[1] for values in expected_values]) else "Соевые бобы"

print(f"Фермеру рекомендуется выращивать {best_crop_overall} вне зависимости от состояния рынка.")
