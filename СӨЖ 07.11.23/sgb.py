import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder

data = pd.read_csv('kazakhstan-food-median-prices-2.csv')

label_encoders = {}  
categorical_columns = ['adm1id', 'catid', 'cmid', 'mktid', 'ptid', 'umid', 'year']
for column in categorical_columns:
    label_encoders[column] = LabelEncoder()
    data[column] = label_encoders[column].fit_transform(data[column])


data = data.dropna(subset=['price'])


features = ['adm1id', 'catid', 'cmid', 'mktid', 'ptid', 'umid', 'year']
X = data[features]
y = data['price']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


model = GradientBoostingRegressor()
model.fit(X_train, y_train)


predictions = model.predict(X_test)

for i in range(len(predictions)):
    print(f'Actual: {y_test.iloc[i]}, Predicted: {predictions[i]}')


mse = mean_squared_error(y_test, predictions)
print('Mean Squared Error:', mse)
