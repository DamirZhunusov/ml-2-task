import pandas as pd
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import random


data = {
    'Volume': [150, 200, 180, 250, 300, 280, 320, 270, 200, 220],
    'Price': [55, 60, 58, 56, 54, 59, 62, 65, 70, 68],
    'Volatility': [0.18, 0.25, 0.22, 0.15, 0.19, 0.21, 0.23, 0.17, 0.24, 0.26],
    'TimeOfDay': ['morning', 'day', 'evening', 'morning', 'day', 'evening', 'morning', 'day', 'evening', 'morning'],
    'TradeType': ['buy', 'sell', 'buy', 'sell', 'buy', 'sell', 'buy', 'sell', 'buy', 'sell'],
    'Success': [1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
}

df = pd.DataFrame(data)


random.seed(42)
df['Success'] = [random.choice([0, 1]) for _ in range(len(df))]

df_encoded = pd.get_dummies(df, columns=['TimeOfDay', 'TradeType'])

X = df_encoded.drop(columns=['Success'])
y = df_encoded['Success']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

dtrain = xgb.DMatrix(X_train, label=y_train)
dtest = xgb.DMatrix(X_test, label=y_test)

params = {
    'objective': 'binary:logistic',  
    'max_depth': 3,  
    'eta': 0.1,  
    'eval_metric': 'logloss'  
}

num_round = 100  
bst = xgb.train(params, dtrain, num_round)

predictions = bst.predict(dtest)
binary_predictions = [1 if x > 0.5 else 0 for x in predictions]

accuracy = accuracy_score(y_test, binary_predictions)
print("Accuracy:", accuracy)
