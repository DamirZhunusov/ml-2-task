
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.impute import SimpleImputer
from sklearn.metrics import mean_squared_error


data = pd.read_csv('kazakhstan-food-median-prices-2.csv')


features = ['category', 'cmname', 'country', 'currency', 'date', 'mktname', 'year']
target = 'price'

X = data[features]
y = data[target]


X = pd.get_dummies(X)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


imputer = SimpleImputer(strategy='mean')
X_train_imputed = pd.DataFrame(imputer.fit_transform(X_train), columns=X_train.columns, index=X_train.index)

X_test_imputed = pd.DataFrame(imputer.transform(X_test), columns=X_test.columns, index=X_test.index)


y_train = y_train.fillna(y_train.mean())
y_test = y_test.fillna(y_test.mean())


model_linear = LinearRegression()
model_rf = RandomForestRegressor()

model_linear.fit(X_train_imputed, y_train)
model_rf.fit(X_train_imputed, y_train)


pred_linear = model_linear.predict(X_test_imputed)
pred_rf = model_rf.predict(X_test_imputed)


weight_linear = 0.5
weight_rf = 0.5


pred_combined = weight_linear * pred_linear + weight_rf * pred_rf

y_test = y_test.dropna()

X_test_imputed = X_test_imputed.loc[~y_test.index.isin(y_test[y_test.isnull()].index)]
pred_combined = pd.Series(pred_combined).loc[~y_test.index.isin(y_test[y_test.isnull()].index)]


mse_combined = mean_squared_error(y_test, pred_combined)
print(f"Mean Squared Error (Combined): {mse_combined}")

print("Предсказания линейной регрессии:", pred_linear)
print("Предсказания случайного леса:", pred_rf)
print("Комбинированные предсказания:", pred_combined)
