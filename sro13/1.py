import pandas as pd
import statsmodels.formula.api as smf

full_health_data = pd.read_csv("data1.csv", header=0, sep=",")

model = smf.ols('Часы_сна ~ Возраст', data = full_health_data)
results = model.fit()
print(results.summary())
