import pandas as pd
import matplotlib.pyplot as plt


data = {
    'Шаги': [8000, 9000, 10000, 8500, 9500, 8700, 9200, 9300, 9800, 9100],
    'Отработанные_часы': [8, 7, 8, 9, 8, 8, 7, 8, 8, 9]
}

your_dataset = pd.DataFrame(data)


your_dataset.plot(x='Шаги', y='Отработанные_часы', kind='scatter')
plt.title('Scatter Plot')
plt.xlabel('Шаги')
plt.ylabel('Отработанные часы')


plt.show()


correlation_your_dataset = your_dataset.corr()
print(correlation_your_dataset)
